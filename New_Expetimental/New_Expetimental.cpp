﻿#include <iostream>

using namespace std;

class Example
{	
public:

	Example() :	Size(10), LastIndex(0)
	{
		p = new int[Size];
	}
	
	void Push_back(int value)
	{	
		
		if (LastIndex >= Size)
		{

			Size *= 2;

			int* Array = new int[Size];


			for (int i = 0; i < LastIndex + 1; i++)
			{
				Array[i] = p[i];
			}


			delete[] p;

			p = Array;
		}
			
		p[LastIndex] = value;

		LastIndex++;

		PrintArray();
	}


	void Pop_back()

	{		
		if (LastIndex >= 0)
		{
		
			int* Array = new int[Size];

			for (int i = 0; i < LastIndex; i++)
			{
				Array[i] = p[i];
			}
			
			delete[] p;

			p = Array;

			LastIndex--;
							
		}

			PrintArray();
	}


	void PrintArray()
	{
		for (int i = 0; i < LastIndex; i++)
		{
			cout << p[i] << "	";
		}	
		cout << endl;
	}


	void LastPrintArray()
	{
		for (int i = 0; i < LastIndex + 1; i++)
		{
			cout << p[i] << "	";
		}
		cout << endl;

		delete[] p;
	}

private:

	
	int* p;

	int Size, LastIndex;
};



int main()
{	
	Example temp;
		
	cout << "Welcome! " << endl;
			
	int Condition = 0;
	
	while (1)
	{
		cout << "Input 1, if you want to add new element. " << endl;
		cout << "Input 2, if you want to remove last element." << endl;
		cout << "Input 3, if you want to finish." << endl;
		cin >> Condition;
		cout << endl;


		if (Condition == 1)
		{
			int Value = 0;

			cout << "Input your element: ";
			cin >> Value;
			cout << endl;

			temp.Push_back(Value);


		}
		else if (Condition == 2)
		{
			temp.Pop_back();
		}
		else
		{
			temp.LastPrintArray();
			break;
		}
	}
}


